Description: Make CMake use system MbedTLS
Author: Andrea Pappacoda <andrea@pappacoda.it>
Forwarded: https://github.com/yuzu-emu/yuzu/pull/7044
Last-Update: 2023-01-07

--- yuzu-0-1176+ds.orig/externals/CMakeLists.txt
+++ yuzu-0-1176+ds/externals/CMakeLists.txt
@@ -36,9 +36,14 @@ if (NOT TARGET inih::INIReader)
     add_subdirectory(inih)
 endif()
 
-# mbedtls
-add_subdirectory(mbedtls EXCLUDE_FROM_ALL)
-target_include_directories(mbedtls PUBLIC ./mbedtls/include)
+# MbedTLS
+find_package(MbedTLS 2.16)
+if(NOT MbedTLS_FOUND)
+    message(STATUS "MbedTLS not found, falling back to externals")
+    add_subdirectory(mbedtls EXCLUDE_FROM_ALL)
+    target_include_directories(mbedcrypto PUBLIC ./mbedtls/include)
+    add_library(MbedTLS::mbedcrypto ALIAS mbedcrypto)
+endif()
 
 # MicroProfile
 add_library(microprofile INTERFACE)
--- /dev/null
+++ yuzu-0-1176+ds/CMakeModules/FindMbedTLS.cmake
@@ -0,0 +1,43 @@
+# SPDX-FileCopyrightText: 2021 Andrea Pappacoda <andrea@pappacoda.it>
+# SPDX-License-Identifier: GPL-2.0-or-later
+
+# MbedTLS 3.0.0 will ship with a CMake package config file,
+# see ARMmbed/mbedtls@d259e347e6e3a630acfc1a811709ca05e5d3b92e,
+# so when yuzu will switch to that version this won't be required anymore.
+#
+# yuzu only uses mbedcrypto, searching for mbedtls and mbedx509 is not
+# needed.
+
+find_path(MbedTLS_INCLUDE_DIR mbedtls/cipher.h)
+
+find_library(MbedTLS_LIBRARY mbedcrypto)
+
+if (MbedTLS_INCLUDE_DIR AND MbedTLS_LIBRARY)
+    # Check for CMAC support
+    include(CheckSymbolExists)
+    set(CMAKE_REQUIRED_LIBRARIES ${MbedTLS_LIBRARY})
+    check_symbol_exists(mbedtls_cipher_cmac ${MbedTLS_INCLUDE_DIR}/mbedtls/cmac.h mbedcrypto_HAS_CMAC)
+    unset(CMAKE_REQUIRED_LIBRARIES)
+
+    # Check if version 2.x is available
+    file(READ "${MbedTLS_INCLUDE_DIR}/mbedtls/version.h" MbedTLS_VERSION_FILE)
+    string(REGEX MATCH "#define[ ]+MBEDTLS_VERSION_STRING[ ]+\"([0-9.]+)\"" _ ${MbedTLS_VERSION_FILE})
+    set(MbedTLS_VERSION "${CMAKE_MATCH_1}")
+
+    if (NOT TARGET MbedTLS::mbedcrypto)
+        add_library(MbedTLS::mbedcrypto UNKNOWN IMPORTED GLOBAL)
+        set_target_properties(MbedTLS::mbedcrypto PROPERTIES
+            IMPORTED_LOCATION "${MbedTLS_LIBRARY}"
+            INTERFACE_INCLUDE_DIRECTORIES "${MbedTLS_INCLUDE_DIR}"
+        )
+    endif()
+endif()
+
+include(FindPackageHandleStandardArgs)
+find_package_handle_standard_args(MbedTLS
+    REQUIRED_VARS
+        MbedTLS_LIBRARY
+        MbedTLS_INCLUDE_DIR
+        mbedcrypto_HAS_CMAC
+    VERSION_VAR MbedTLS_VERSION
+)
--- yuzu-0-1176+ds.orig/src/core/CMakeLists.txt
+++ yuzu-0-1176+ds/src/core/CMakeLists.txt
@@ -783,7 +783,7 @@ endif()
 create_target_directory_groups(core)
 
 target_link_libraries(core PUBLIC common PRIVATE audio_core network video_core)
-target_link_libraries(core PUBLIC Boost::boost PRIVATE fmt::fmt nlohmann_json::nlohmann_json mbedtls Opus::opus)
+target_link_libraries(core PUBLIC Boost::boost PRIVATE fmt::fmt nlohmann_json::nlohmann_json MbedTLS::mbedcrypto Opus::opus)
 if (MINGW)
     target_link_libraries(core PRIVATE ${MSWSOCK_LIBRARY})
 endif()
--- yuzu-0-1176+ds.orig/src/dedicated_room/CMakeLists.txt
+++ yuzu-0-1176+ds/src/dedicated_room/CMakeLists.txt
@@ -16,7 +16,7 @@ if (ENABLE_WEB_SERVICE)
     target_link_libraries(yuzu-room PRIVATE web_service)
 endif()
 
-target_link_libraries(yuzu-room PRIVATE mbedtls mbedcrypto)
+target_link_libraries(yuzu-room PRIVATE MbedTLS::mbedcrypto)
 if (MSVC)
     target_link_libraries(yuzu-room PRIVATE getopt)
 endif()
